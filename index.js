class viajes {

    //Atributos

    medioParticular1;
    medioParticular2;
    servicioPublico1;
    servicioPublico2;
    sinTransporte;

    //Metodos

    constructor (a,b,c,d,e){
        this.medioParticular1 = a;
        this.medioParticular2 = b;
        this.servicioPublico1 = c;
        this.servicioPublico2 = d;
        this.sinTransporte = e;
    }

    Particular1(){
        let opcion1 =  console.log("Usted se transporta por medio de: " + this.medioParticular1);
        let q = console.log("Los costos al elegir un medio de transporte particular como la " + this.medioParticular1 , "son: La gasolina, los viaticos, los peajes y la tecnicomecanica");
        return opcion1;
    }

    Particular2(){
        let opcion2 =  console.log("Usted se transporta por medio de: " + this.medioParticular2);
        let q = console.log("Los costos al elegir un medio de transporte particular como el " + this.medioParticular2 , "son: La gasolina, los viaticos, los peajes y la tecnicomecanica");
        return opcion2;
    }

    Publico1(){
        let opcion3;
        opcion3 =  console.log("Usted se transporta por medio: " + this.servicioPublico1);
        let q = console.log("Los costos al elegir un medio de transporte publico como el " + this.servicioPublico1 , "son: La comida, el equipaje, y los pasajes");
        return opcion3;
    }

    Publico2(){
        let opcion4;
        opcion4 = console.log("Usted se transporte por medio: " + this.servicioPublico2);
        let q = console.log("Los costos al elegir un medio de transporte publico como el " + this.servicioPublico2 , "son: La comida, el equipaje, y los pasajes");
        return opcion4;
    }

    SinTransporte(){
        let opcion5;
        opcion5 =  console.log("Usted no utiliza ningun medio de transporte : " + this.sinTransporte);
        let q = console.log("Los costos al no elegir un medio de transporte, para viajar  " + this.sinTransporte , "son: La comida, el hotel, y la seguridad");
        return opcion5;
    }

   costos1Moto(){
        this.gasolina1();
        this.viaticos1();
        this.peajes1();
        this.tecnicomecanica1();
    }
    
    costos2Carro(){
        this.gasolina2();
        this.viaticos2();
        this.peajes2();
        this.tecnicomecanica2();
    }

    costos3Flota(){
        this.comida1();
        this.equipaje1();
        this.pasajes1();
    }

    costos4Avion(){
        this.comida2();
        this.equipaje2();
        this.pasajes2();
    }

    costos5Caminar(){
        this.comida();
        this.hotel();
        this.seguridad();
    }

}

class viajeMedioParticular extends viajes {
    gasolina1(){
        var f = console.log("Calculando el costo de la gasolina........" );
        var g = console.log("El costo de la gasolina para la: " + this.medioParticular1,"es de $$$$$$$");
    return;
    }
    viaticos1(){
        var f = console.log("Calculando el costo del los viaticos........" );
        var g = console.log("El costo de los viaticos para la: " + this.medioParticular1,"es de $$$$$$$");
        return;
    }
    peajes1(){
        var f = console.log("Calculando el costo del los peajes........" );
        var g = console.log("El costo de los peajes para la: " + this.medioParticular1,"es de $$$$$$$");
        return;
    }
    tecnicomecanica1(){
        var f = console.log("Calculando el costo de la revision tecnicomecanica........" );
        var g = console.log("El costo de la revision para la: " + this.medioParticular1,"es de $$$$$$$");
        return;
    }

    gasolina2(){
        var f = console.log("Calculando el costo de la gasolina........" );
        var g = console.log("El costo de la gasolina para el: " + this.medioParticular2,"es de $$$$$$$");
    return;
    }
    viaticos2(){
        var f = console.log("Calculando el costo del los viaticos........" );
        var g = console.log("El costo de los viaticos para el: " + this.medioParticular2,"es de $$$$$$$");
        return;
    }
    peajes2(){
        var f = console.log("Calculando el costo del los peajes........" );
        var g = console.log("El costo de los peajes para el: " + this.medioParticular2,"es de $$$$$$$");
        return;
    }
    tecnicomecanica2(){
        var f = console.log("Calculando el costo de la revision tecnicomecanica........" );
        var g = console.log("El costo de la revision para el: " + this.medioParticular2,"es de $$$$$$$");
        return;
    }
}
let miViajeMedioParticular = new viajeMedioParticular("moto","carro");

class viajeServicioPublico extends viajes {
    comida1(){
        var f = console.log("Calculando el costo de la comida........" );
        var g = console.log("El costo de la comida para la: " + this.servicioPublico1,"es de $$$$$$$");
    return;
    }
    equipaje1(){
        var f = console.log("Calculando el costo del equipaje........" );
        var g = console.log("El costo del equipaje para el: " + this.servicioPublico1,"es de $$$$$$$");
        return;
    }
    pasajes1(){
        var f = console.log("Calculando el costo del los pasajes........" );
        var g = console.log("El costo del los pasajes para el: " + this.servicioPublico1,"es de $$$$$$$");
        return;
    }

    comida2(){
        var f = console.log("Calculando el costo de la comida........" );
        var g = console.log("El costo de la comida para el: " + this.servicioPublico2,"es de $$$$$$$");
    return;
    }
    equipaje2(){
        var f = console.log("Calculando el costo del equipaje........" );
        var g = console.log("El costo del equipaje para el: " + this.servicioPublico2,"es de $$$$$$$");
        return;
    }
    pasajes2(){
        var f = console.log("Calculando el costo del los pasajes........" );
        var g = console.log("El costo del los pasajes para el: " + this.servicioPublico2,"es de $$$$$$$");
        return;
    }
}
let miViajeServicioPublico = new viajeServicioPublico(" "," ","flota","avion");

class viajejSinTransporte extends viajes{  
    comida(){
        var f = console.log("Calculando el costo de la comida........" );
        var g = console.log("El costo de la comida por ir: " + this.sinTransporte,"es de $$$$$$$");
    return;
    }
    hotel(){
        var f = console.log("Calculando el costo del hotel........" );
        var g = console.log("El costo del hotel por ir: " + this.sinTransporte,"es de $$$$$$$");
        return;
    }
    seguridad(){
        var f = console.log("Calculando el costo del la seguridad........" );
        var g = console.log("El costo de la seguridad por ir: " + this.sinTransporte,"es de $$$$$$$");
        return;
    }
}
let miViajeSinTransporte =  new viajejSinTransporte(" "," "," "," ","caminando");





  


